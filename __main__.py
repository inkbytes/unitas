import logging
import pandas as pd
import os
import tinydb as db
from inkbytes.common.system import csv_json_handler as _cj_
from inkbytes.common.system.config_loader import ConfigLoader


def main():
    dfx = _cj_.unify_jsondb_files_to_df(input_path)
    if dfx.empty:
        print("empty dataframe")
    else:
        original_df = dfx[dfx['text'].str.split().apply(len) >= 150]
        original_df = original_df.drop_duplicates(subset="uid")
        unify_articles_files(original_df)


def unify_articles_files(dataframe: pd.DataFrame):
    articles: [] = []
    watermark = _cj_.filename_watermark()
    unified_db = db.TinyDB(os.path.join(output_path, f'unified_articles.{watermark}.tdb.json'))
    for index, row in dataframe.iterrows():
        document = row.to_dict()
        articles.append(document)
    if len(articles) > 0:
        try:
            unified_db.insert_multiple(articles)
        except ValueError as e:
            logger.error(f"Error inserting articles into database")
    _cj_.clean_staging_db_files(input_path, output_history_path)


# ----------------------------------------------------------------------------------
if __name__ == '__main__':
    module_name = "Inkbytes Unitas 1.0"
    sysdictionary = ConfigLoader('./env.yaml')
    # Access the environment variables
    LOG_LEVEL = sysdictionary.__('logging.level')
    input_path = sysdictionary.__('storage.input_path')
    output_path = sysdictionary.__('storage.output_path')
    output_history_path = sysdictionary.__('storage.output_history_path')
    logger = logging.getLogger(module_name)
    logger.setLevel(LOG_LEVEL)
    main()
